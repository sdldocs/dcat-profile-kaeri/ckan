# Extending Guide

## Writing extensions tutorial
이 튜토리얼에서는 간단한 CKAN 확장을 만드는 과정을 안내하고, 그 과정에서 CKAN 확장 개발자가 알아야 할 핵심 개념을 소개한다. 예를 들어, CCAN과 함께 패키지된 `example_iauthfunctions` 확장을 사용한다. 이는 일부 CKAN의 권한 부여 규칙을 커스터마이즈하는 단순한 CKAN 확장이다.

### Installing CKAN

### Creating a new extension

### Creating a plugin class

### Adding the plugin to `setup.py`

### Installing the extension

### Enabling the plugin

### Implementing the 'iAUthFunctions` plugin interface

### Using the plugins toolkit

### Exception handling

### We're done!

## Using custom config settings in extensions

## Making configuration options

## Testing extensions

## Best Practices for writing extensions

## Customizing dataset and resource metadata fields using IDatasetForm
CKAN의 기본 메타데이터를 넘어 데이터 세트에 대한 추가 메타데이터를 저장하는 것이 일반적인 사용 사례이다. CKAN은 데이터 세트를 생성하거나 업데이트할 때 데이터 세트에 대해 임의 키/값 쌍을 저장할 수 있는 간단한 방법을 제공한다. 이러한 정보는 API를 통해 액세스할 때 웹 인터페이스의 "추가 정보" 섹션과 JSON의 '추가 정보' 필드에 나타난다.

기본 추가 기능은 키와 값에 대한 문자열만 사용할 수 있으며, 입력에는 유효성 검사가 적용되지 않으므로 입력을 필수로 지정하거나 가능한 값을 정의된 목록으로 제한할 수 없다. CKAN 플러그인은 CKAN의 IDatasetForm 플러그인 인터페이스를 사용하여 커스텀 first-class(?) 메타데이터 필드를 CKAN 데이터 세트에 추가하고 이러한 필드에 대한 커스텀 검증을 수행할 수 있다.

### CKAN schemas and validation
When a dataset is created, updated or viewed, the parameters passed to CKAN (e.g. via the web form when creating or updating a dataset, or posted to an API end point) are validated against a schema. For each parameter, the schema will contain a corresponding list of functions that will be run against the value of the parameter. Generally these functions are used to validate the value (and raise an error if the value fails validation) or convert the value to a different value.

For example, the schemas can allow optional values by using the `ignore_missing()` validator or check that a dataset exists using `package_id_exists()`. A list of available validators can be found at the [Validator functions reference](). You can also define your own [Custom validators]().

We will be customizing these schemas to add our additional fields. The `IDatasetForm` interface allows us to override the schemas for creation, updating and displaying of datasets.

| function | description |
|----------|-------------|
| `create_package_schema()` | Return the schema for validating new dataset dicts. |
| `update_package_schema()` | Return the schema for validating updated dataset dicts. |
| `show_package_schema()` | Return a schema to validate datasets before they're shown to the user. |
| `is_fallback()` | Return `True` if this plugin is the fallback plugin. |
| `package_types()` | Return an iterable of dataset (package) types that this plugin handles. |

CKAN allows you to have multiple IDatasetForm plugins, each handling different dataset types. So you could customize the CKAN web front end, for different types of datasets. In this tutorial we will be defining our plugin as the fallback plugin. This plugin is used if no other IDatasetForm plugin is found that handles that dataset type.

The IDatasetForm also has other additional functions that allow you to provide a custom template to be rendered for the CKAN frontend, but we will not be using them for this tutorial.

### Adding custom fields to dataset
Create a new plugin named `ckanext-extrafields` and create a class named `ExampleIDatasetFormPlugins` inside `ckanext-extrafields/ckanext/extrafields/plugin.py` that implements the `IDatasetForm` interface and inherits from `SingletonPlugin` and `DefaultDatasetForm`.

```python
# /usr/lib/ckan/default/src/ckanext-extrafields/ckanext/extrafields/plugin.py
# encoding: utf-8

from __future__ import annotations

from ckan.types import Schema
import ckan.plugins as p
import ckan.plugins.toolkit as tk


class ExampleIDatasetFormPlugin(tk.DefaultDatasetForm, p.SingletonPlugin):
    p.implements(p.IDatasetForm)
```

### Updating the CKAN schema
The `create_package_schema()` function is used whenever a new dataset is created, we’ll want update the default schema and insert our custom field here. We will fetch the default schema defined in `default_create_package_schema()` by running `create_package_schema()`’s super function and update it.

```python
# where?
    def create_package_schema(self) -> Schema:
        # let's grab the default schema in our plugin
        schema: Schema = super(
            ExampleIDatasetFormPlugin, self).create_package_schema()
        # our custom field
        schema.update({
            'custom_text': [tk.get_validator('ignore_missing'),
                            tk.get_converter('convert_to_extras')]
        })
        return schema
```

The CKAN schema is a dictionary where the key is the name of the field and the value is a list of validators and converters. Here we have a validator to tell CKAN to not raise a validation error if the value is missing and a converter to convert the value to and save as an extra. We will want to change the `update_package_schema()` function with the same update code.

```python
    def update_package_schema(self) -> Schema:
        schema: Schema = super(
            ExampleIDatasetFormPlugin, self).show_package_schema()
        # our custom field
        schema.update({
            'custom_text': [tk.get_validator('ignore_missing'),
                            tk.get_converter('convert_to_extras')]
        })
        return schema
```

The `show_package_schema()` is used when the `package_show()` action is called, we want the *default_show_package_schema* to be updated to include our custom field. This time, instead of converting to an extras field, we want our field to be converted from an extras field. So we want to use the `convert_from_extras()` converter.

```python
    def show_package_schema(self) -> Schema:
        schema: Schema = super(
            ExampleIDatasetFormPlugin, self).show_package_schema()
        schema.update({
            'custom_text': [tk.get_converter('convert_from_extras'),
                            tk.get_validator('ignore_missing')]
        })
        return schema
```

### Dataset types
The `package_types()` function defines a list of dataset types that this plugin handles. Each dataset has a field containing its type. Plugins can register to handle specific types of dataset and ignore others. Since our plugin is not for any specific type of dataset and we want our plugin to be the default handler, we update the plugin code to contain the following:

```python
    def is_fallback(self):
        # Return True to register this plugin as the default handler for
        # package types not handled by any other IDatasetForm plugin.
        return True

    def package_types(self) -> list[str]:
        # This plugin doesn't handle any special package types, it just
        # registers itself as the default (above).
        return []
```

### Updating templates
In order for our new field to be visible on the CKAN front-end, we need to update the templates. Add an additional line to make the plugin implement the *IConfigurer* interface

```python
class ExampleIDatasetFormPlugin(tk.DefaultDatasetForm, p.SingletonPlugin):
    p.implements(p.IDatasetForm)
    p.implements(p.IConfigurer)
```

This interface allows to implement a function `update_config()` that allows us to update the CKAN *config, in our case we want to add an additional location for CKAN to look for templates. Add the following code to your plugin.

```python
    def update_config(self, config: CKANConfig):
        # Add this plugin's templates dir to CKAN's extra_template_paths, so
        # that CKAN will use this plugin's custom templates.
        tk.add_template_directory(config, 'templates')
```

You will also need to add a directory under your extension directory to store the templates. Create a directory called `ckanext-extrafields/ckanext/extrafields/templates/` and the subdirectories `ckanext-extrafields/ckanext/extrafields/templates/package/snippets/` .

We need to override a few templates in order to get our custom field rendered. A common option when using a custom schema is to remove the default custom field handling that allows arbitrary key/value pairs. Create a template file in our templates directory called `package/snippets/package_metadata_fields.html` containing

```html
{% ckan_extends %}

{# You could remove 'free extras' from the package form like this, but we keep them for this example's tests.
  {% block custom_fields %}
  {% endblock %}
#}
```

This overrides the custom_fields block with an empty block so the default CKAN custom fields form does not render.

*New in version 2.3*: Starting from CKAN 2.3 you can combine free extras with custom fields handled with `convert_to_extras` and `convert_from_extras`. On prior versions you’ll always need to remove the free extras handling.

Next add a template in our template directory called `package/snippets/package_basic_fields.html` containing

```html
{% ckan_extends %}

{% block package_basic_fields_custom %}
  {{ form.input('custom_text', label=_('Custom Text'), id='field-custom_text', placeholder=_('custom text'), value=data.custom_text, error=errors.custom_text, classes=['control-medium']) }}
{% endblock %}
```

This adds our custom_text field to the editing form. Finally we want to display our custom_text field on the dataset page. Add another file called `package/snippets/additional_info.html` containing

```html
{% ckan_extends %}

{% block extras %}
  {% if pkg_dict.custom_text %}
    <tr>
      <th scope="row" class="dataset-label">{{ _("Custom Text") }}</th>
      <td class="dataset-details">{{ pkg_dict.custom_text }}</td>
    </tr>
  {% endif %}
{% endblock %}
```

This template overrides the default extras rendering on the dataset page and replaces it to just display our custom field.

You’re done! Make sure you have your plugin installed and setup as in the *extension/tutorial*. Then run a development server and you should now have an additional field called “Custom Text” when displaying and adding/editing a dataset.

### Cleaning up the code
Before we continue further, we can clean up the `create_package_schema()` and `update_package_schema()`. There is a bit of duplication that we could remove. Replace the two functions with:

```python
    def _modify_package_schema(self, schema: Schema) -> Schema:
        schema.update({
            'custom_text': [tk.get_validator('ignore_missing'),
                            tk.get_converter('convert_to_extras')]
        })
        return schema

    def create_package_schema(self):
        schema: Schema = super(
            ExampleIDatasetFormPlugin, self).create_package_schema()
        schema = self._modify_package_schema(schema)
        return schema

    def update_package_schema(self):
        schema: Schema = super(
            ExampleIDatasetFormPlugin, self).update_package_schema() # ? show_package_schema()
        schema = self._modify_package_schema(schema)
        return schema
```

### Custom validator
You may define custom validators in your extensions and you can share validators between extensions by registering them with the `IValidators` interface.

Any of the following objects may be used as validators as part of a custom dataset, group or organization schema. CKAN’s validation code will check for and attempt to use them in this order:

1. a function taking a single parameter: `validator(value)`
1. a function taking four parameters: `validator(key, flattened_data, errors, context)`
1. a function taking two parameters: `validator(value, context)`

#### `validator(value)`
The simplest form of validator is a callable taking a single parameter. For example:

```python
from ckan.plugins.toolkit import Invalid

def starts_with_b(value):
    if not value.startswith('b'):
        raise Invalid("Doesn't start with b")
    return value
```

The `starts_with_b` validator causes a validation error for values not starting with ‘b’. On a web form this validation error would appear next to the field to which the validator was applied.

`return value` must be used by validators when accepting data or the value will be converted to *None*. This form is useful for converting data as well, because the return value will replace the field value passed:

```python
def embiggen(value):
    return value.upper()
```

The `embiggen` validator will convert values passed to all-uppercase.

#### `validator(value, context)`
Validators that need access to the database or information about the user may be written as a callable taking two parameters. `context['session']` is the sqlalchemy session object and `context['user']` is the username of the logged-in user:

Otherwise this is the same as the single-parameter form above.

#### `validator(key, flattened_data, errors, context)`
Validators that need to access or update multiple fields may be written as a callable taking four parameters.

All fields and errors in a `flattened` form are passed to the validator. The validator must fetch values from `flattened_data` and may replace values in `flattened_data`. The return value from this function is ignored.

`key` is the flattened key for the field to which this validator was applied. For example `('notes',)` for the dataset notes field or `('resources', 0, 'url')` for the url of the first resource of the dataset. These flattened keys are the same in both the `flattened_data` and `errors` dicts passed.

`errors` contains lists of validation errors for each field.

`context` is the same value passed to the two-parameter form above.

Note that this form can be tricky to use because some of the values in `flattened_data` will have had validators applied but other fields won’t. You may add this type of validator to the special schema fields `'__before'` or `'__after'` to have them run before or after all the other validation takes place to avoid the problem of working with partially-validated data.

The validator has to be registered. Example:

```python
from ckan import plugins


class ExampleIValidatorsPlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IValidators)

    def get_validators(self) -> dict[str, Validator]:
        return {
            u'equals_fortytwo': equals_fortytwo,
            u'negate': negate,
            u'unicode_only': unicode_please,
        }
```

### Tag vocabularies
If you need to add a custom field where the input options are restricted to a provided list of options, you can use tag vocabularies [Tag Vocabularies](). We will need to create our vocabulary first. By calling `vocabulary_create()`. Add a function to your `plugin.py` above your plugin class.

```python

def create_country_codes():
    user = tk.get_action('get_site_user')({'ignore_auth': True}, {})
    context: Context = {'user': user['name']}
    try:
        data = {'id': 'country_codes'}
        tk.get_action('vocabulary_show')(context, data)
    except tk.ObjectNotFound:
        data = {'name': 'country_codes'}
        vocab = tk.get_action('vocabulary_create')(context, data)
        for tag in (u'uk', u'ie', u'de', u'fr', u'es'):
            data: dict[str, Any] = {'name': tag, 'vocabulary_id': vocab['id']}
            tk.get_action('tag_create')(context, data)
```

This code block is taken from the `example_idatsetform` plugin. `create_country_codes` tries to fetch the vocabulary country_codes using `vocabulary_show()`. If it is not found it will create it and iterate over the list of countries ‘uk’, ‘ie’, ‘de’, ‘fr’, ‘es’. For each of these a vocabulary tag is created using `tag_create()`, belonging to the vocabulary `country_code`.

Although we have only defined five tags here, additional tags can be created at any point by a sysadmin user by calling `tag_create()` using the API or action functions. Add a second function below `create_country_codes`

```python
def country_codes():
    create_country_codes()
    try:
        tag_list = tk.get_action('tag_list')
        country_codes = tag_list({}, {'vocabulary_id': 'country_codes'})
        return country_codes
    except tk.ObjectNotFound:
        return None
```

country_codes will call `create_country_codes` so that the `country_codes` vocabulary is created if it does not exist. Then it calls `tag_list()` to return all of our vocabulary tags together. Now we have a way of retrieving our tag vocabularies and creating them if they do not exist. We just need our plugin to call this code.

#### Adding tags to the schema
Update `_modify_package_schema()` and `show_package_schema()`

```python

    def _modify_package_schema(self, schema: Schema):
        schema.update({
            'custom_text': [tk.get_validator('ignore_missing'),
                            tk.get_converter('convert_to_extras')]
        })
        schema.update({
            'country_code': [
                tk.get_validator('ignore_missing'),
                cast(ValidatorFactory,
                     tk.get_converter('convert_to_tags'))('country_codes'),
            ]
        })
        return schema

    def show_package_schema(self) -> Schema:
        schema: Any = super(
            ExampleIDatasetFormPlugin, self).show_package_schema()
        schema.update({
            'custom_text': [tk.get_converter('convert_from_extras'),
                            tk.get_validator('ignore_missing')]
        })

        schema['tags']['__extras'].append(tk.get_converter('free_tags_only'))
        schema.update({
            'country_code': [
                cast(ValidatorFactory,
                     tk.get_converter('convert_from_tags'))('country_codes'),
                tk.get_validator('ignore_missing')]
        })
        return schema

```

We are adding our tag to our plugin’s schema. A converter is required to convert the field in to our tag in a similar way to how we converted our field to extras earlier. In `show_package_schema()` we convert from the tag back again but we have an additional line with another converter containing `free_tags_only()`. We include this line so that vocab tags are not shown mixed with normal free tags.

#### Adding tags to templates
Add an additional plugin.implements line to to your plugin to implement the `ITemplateHelpers`, we will need to add a `get_helpers()` function defined for this interface.

```python
    p.implements(p.ITemplateHelpers)

    def get_helpers(self):
        return {'country_codes': country_codes}

```

Our intention here is to tie our country_code fetching/creation to when they are used in the templates. Add the code below to `package/snippets/package_metadata_fields.html`

```html

{% block package_metadata_fields %}

  <div class="control-group">
    <label class="form-label" for="field-country_code">{{ _("Country Code") }}</label>
    <div class="controls">
      <select id="field-country_code" name="country_code" data-module="autocomplete">
        {% for country_code in h.country_codes()  %}
          <option value="{{ country_code }}" {% if country_code in data.get('country_code', []) %}selected="selected"{% endif %}>{{ country_code }}</option>
        {% endfor %}
      </select>
    </div>
  </div>

  {{ super() }}

{% endblock %}
```

This adds our country code to our template, here we are using the additional helper country_codes that we defined in our get_helpers function in our plugin.

### Adding custom fields to resource
In order to customize the fields in a resource the schema for resources needs to be modified in a similar way to the datasets. The resource schema is nested in the dataset dict as package[‘resources’]. We modify this dict in a similar way to the dataset schema. Change `_modify_package_schema` to the following.

```python
    def _modify_package_schema(self, schema: Schema):
        # Add our custom country_code metadata field to the schema.

        schema.update({
                'country_code': [
                    tk.get_validator('ignore_missing'),
                    cast(
                        ValidatorFactory,
                        tk.get_converter('convert_to_tags'))('country_codes')]
                })
        # Add our custom_test metadata field to the schema, this one will use
        # convert_to_extras instead of convert_to_tags.
        schema.update({
                'custom_text': [tk.get_validator('ignore_missing'),
                    tk.get_converter('convert_to_extras')]
                })
        # Add our custom_resource_text metadata field to the schema
        cast(Schema, schema['resources']).update({
                'custom_resource_text' : [ tk.get_validator('ignore_missing') ]
                })
        return schema
```

Update `show_package_schema()` similarly

```python
    def show_package_schema(self) -> Schema:
        schema: Schema = super(
            ExampleIDatasetFormPlugin, self).show_package_schema()

        # Don't show vocab tags mixed in with normal 'free' tags
        # (e.g. on dataset pages, or on the search page)
        _extras = cast("list[Validator]",
                       cast(Schema, schema['tags'])['__extras'])
        _extras.append(tk.get_converter('free_tags_only'))

        # Add our custom country_code metadata field to the schema.
        schema.update({
            'country_code': [
                cast(
                    ValidatorFactory,
                    tk.get_converter('convert_from_tags'))('country_codes'),
                tk.get_validator('ignore_missing')]
            })

        # Add our custom_text field to the dataset schema.
        schema.update({
            'custom_text': [tk.get_converter('convert_from_extras'),
                tk.get_validator('ignore_missing')]
            })

        cast(Schema, schema['resources']).update({
                'custom_resource_text' : [ tk.get_validator('ignore_missing') ]
            })
        return schema
```

Add the code below to `package/snippets/resource_form.html`

```html
{% ckan_extends %}

{% block basic_fields_url %}
{{ super() }}

  {{ form.input('custom_resource_text', label=_('Custom Text'), id='field-custom_resource_text', placeholder=_('custom resource text'), value=data.custom_resource_text, error=errors.custom_resource_text, classes=['control-medium']) }}
{% endblock %}
```

This adds our custom_resource_text to the editing form of the resources.

Save and reload your development server CKAN will take any additional keys from the resource schema and save them the its extras field. The templates will automatically check this field and display them in the resource_read page.

### Sorting by custom fields on the dataset search page
Now that we’ve added our custom field, we can customize the CKAN web front end search page to sort datasets by our custom field. Add a new file called `ckanext-extrafields/ckanext/extrafields/templates/package/search.html` containing:

```html
{% ckan_extends %}

{% block form %}
  {% set facets = {
    'fields': fields_grouped,
    'search': search_facets,
    'titles': facet_titles,
    'translated_fields': translated_fields,
    'remove_field': remove_field }
  %}
  {% set sorting = [
    (_('Relevance'), 'score desc, metadata_modified desc'),
    (_('Name Ascending'), 'title_string asc'),
    (_('Name Descending'), 'title_string desc'),
    (_('Last Modified'), 'metadata_modified desc'),
    (_('Custom Field Ascending'), 'custom_text asc'),
    (_('Custom Field Descending'), 'custom_text desc'),
    (_('Popular'), 'views_recent desc') if g.tracking_enabled else (false, false) ]
  %}
  {% snippet 'snippets/search_form.html', type='dataset', query=q, sorting=sorting, sorting_selected=sort_by_selected, count=page.item_count, facets=facets, show_empty=request.args, error=query_error %}
{% endblock %}
```

This overrides the search ordering drop down code block, the code is the same as the default dataset search block but we are adding two additional lines that define the display name of that search ordering (e.g. Custom Field Ascending) and the SOLR sort ordering (e.g. custom_text asc). If you reload your development server you should be able to see these two additional sorting options on the dataset search page.

The SOLR sort ordering can define arbitrary functions for custom sorting, but this is beyond the scope of this tutorial for further details see http://wiki.apache.org/solr/CommonQueryParameters#sort and http://wiki.apache.org/solr/FunctionQuery

You can find the complete source for this tutorial at https://github.com/ckan/ckan/tree/master/ckanext/example_idatasetform

## Plugin interface reference

## Plugin toolkits reference

## Validator functions reference

## Internationalizing strings in extensions
> **See Also**
> In order to internationalize your extension you must [mark its strings for internationalization](). See also [Translating CKAN]().

> This tutorial assumes that you have read the [Writing extensions tutorial]().

We will create a simple extension to demonstrate the translation of strings inside extensions. After running:

```bash
ckan -c |ckan.ini| create -t ckanext ckanext-itranslation
```

Change the `plugin.py` file to:

```python
# /usr/lib/ckan/default/src/ckanext-itranslation/ckanext/itranslation/plugin.py
# encoding: utf-8

from ckan.common import CKANConfig
from ckan import plugins
from ckan.plugins import toolkit


class ExampleITranslationPlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IConfigurer)

    def update_config(self, config: CKANConfig):
        toolkit.add_template_directory(config, 'templates')
```

Add a template file `ckanext-itranslation/templates/home/index.html` containing:

```html
% ckan_extends %}

{% block primary_content %}
{% trans %}This is an untranslated string{% endtrans %}
{% endblock %}
```

This template provides a sample string that we will internationalize in this tutorial.

> **Note**
> While this tutorial only covers Python/Jinja templates it is also possible (since CKAN 2.7) to [translate strings in an extension’s JavaScript modules]().

### Extract strings
> Tip
> If you have generated a new extension whilst following this tutorial the default template will have generated these files for you and you can simply run the `extract_messages` command immediately.

Check your `setup.py` file in your extension for the following lines

```python

setup(
    entry_points='''
        [ckan.plugins]
        itranslation=ckanext.itranslation.plugin:ExampleITranslationPlugin
        [babel.extractors]
        ckan = ckan.lib.extract:extract_ckan
    '''

    message_extractors={
        'ckanext': [
            ('**.py', 'python', None),
            ('**.js', 'javascript', None),
            ('**/templates/**.html', 'ckan', None),
        ],
    }

```

These lines will already be present in our example, but if you are adding internationalization to an older extension, you may need to add them. If you have your templates in a directory differing from the default location (`ckanext/yourplugin/i18n`), you may need to change the `message_extractors` stanza. You can read more about message extractors in the [babel documentation]().

Add a directory to store your translations:

```bash
mkdir ckanext-itranslations/ckanext/itranslations/i18n
```

Next you will need a babel config file. Add a `setup.cfg` file containing the following (make sure you replace `itranslations` with the name of your extension):

```ini
[extract_messages]
keywords = translate isPlural
add_comments = TRANSLATORS:
output_file = ckanext/itranslation/i18n/ckanext-itranslation.pot
width = 80

[init_catalog]
domain = ckanext-itranslation
input_file = ckanext/itranslation/i18n/ckanext-itranslation.pot
output_dir = ckanext/itranslation/i18n

[update_catalog]
domain = ckanext-itranslation
input_file = ckanext/itranslation/i18n/ckanext-itranslation.pot
output_dir = ckanext/itranslation/i18n

[compile_catalog]
domain = ckanext-itranslation
directory = ckanext/itranslation/i18n
statistics = true
```

This file tells babel where the translation files are stored. You can then run the `extract_messages` command to extract the strings from your extension:

```bash
python setup.py extract_messages
```

This will create a template PO file named `ckanext/itranslations/i18n/ckanext-itranslation.pot`.

At this point, you can either upload and manage your translations using Transifex or manually edit your translations.

### Manually create translations
We will create translation files for the `fr` locale. Create the translation PO files for the locale that you are translating for by running [init_catalog](https://babel.pocoo.org/en/latest/setup.html#init-catalog):

```bash
python setup.py init_catalog -l fr
```

This will generate a file called `i18n/fr/LC_MESSAGES/ckanext-itranslation.po`. This file should contain the untranslated string on our template. You can manually add a translation for it by editing the `msgstr` section:

```
msgid "This is an untranslated string"
msgstr "This is a itranslated string"
```

### Translation with Transifex
Once you have created your translations, you can manage them using Transifex. This is out side of the scope of this tutorial, but the Transifex documentation provides tutorials on how to [upload translations]() and how to manage them using the [command line client]().

### Compiling the catalog
Once the translation files (`po`) have been updated, either manually or via Transifex, compile them by running:

```bash
python setup.py compile_catalog
```

This will generate a `mo` file containing your translations that can be used by CKAN.

### ITranslation interface
Once you have created the translated strings, you will need to inform CKAN that your extension is translated by implementing the `ITranslation` interface in your extension. Edit your `plugin.py` to contain the following.

```python
# /usr/lib/ckan/default/src/ckanext-itranslation/ckanext/itranslation/plugin.py (?)
# encoding: utf-8

from ckan.common import CKANConfig
from ckan import plugins
from ckan.plugins import toolkit
from ckan.lib.plugins import DefaultTranslation


class ExampleITranslationPlugin(plugins.SingletonPlugin, DefaultTranslation):
    plugins.implements(plugins.ITranslation)
    plugins.implements(plugins.IConfigurer)

    def update_config(self, config: CKANConfig):
        toolkit.add_template_directory(config, 'templates')
```

You’re done! To test your translated extension, make sure you add the extension to your /etc/ckan/default/ckan.ini, run a `ckan run` command and browse to http://localhost:5000. You should find that switching to the `fr` locale in the web interface will change the home page string to `this is an itranslated string`.

#### Advanced ITranslation usage
If you are translating a CKAN extension that already exists, or you have structured your extension differently from the default layout. You may have to tell CKAN where to locate your translated files, you can do this by not having your plugin inherit from the `DefaultTranslation` class and instead implement the `ITranslation` interface yourself.

| function | description |
|----------|-------------|
| `i18n_directory()` | Change the directory of the .mo translation files |
| `i18n_locales()` | Change the list of locales that this plugin handles |
| `i18n_domain()` | Change the gettext domain handled by this plugin |

## Migration from Pylons to Flask

## Signals

